from setuptools import setup

setup(
    name='package_hyJfRPylm',
    version='0.1.0',
    description='A Python package for data analysis homework 2',
    url='https://gitlab.com/1ncend1ary/package_hyJfRPylm',
    author='Oleg Hools (not a real name)',
    author_email='ohools@mail.ru',
    license='MIT',
    packages=['package_hyJfRPylm'],
    install_requires=['numpy<1.16.5',
                      ],

    classifiers=[],
)
